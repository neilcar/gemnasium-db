---
identifier: "GMS-2022-5891"
identifiers:
- "GHSA-cr84-xvw4-qx3c"
- "GMS-2022-5891"
- "CVE-2022-25918"
package_slug: "npm/shescape"
title: "Inefficient Regular Expression Complexity in shescape "
description: "### Impact\n\nThis impacts users that use shescape to escape arguments:\n\n-
  for the Unix shell Bash, or any not-officially-supported Unix shell;\n- using the
  `escape` or `escapeAll` functions with the `interpolation` option set to `true`.\n\nAn
  attacker can cause polynomial backtracking in terms of the input string length due
  to a Regular Expression in shescape that is vulnerable to Regular Expression Denial
  of Service (ReDoS). Example:\n\n```javascript\nimport * as shescape from \"shescape\";\n\n/*
  1. Prerequisites */\nconst options = {\n interpolation: true,\n // and\n shell:
  \"/bin/bash\",\n // or\n shell: \"some-not-officially-supported-shell\",\n // or\n
  shell: undefined, // Only if the system's default shell is bash or an unsupported
  shell.\n};\n\n/* 2. Attack */\nlet userInput = '{,'.repeat(150_000); // polynomial
  backtracking\n\n/* 3. Usage */\nshescape.escape(userInput, options);\n// or\nshescape.escapeAll([userInput],
  options);\n```\n\n### Patches\n\nThis bug has been patched in [v1.6.1](https://github.com/ericcornelissen/shescape/releases/tag/v1.6.1)
  which you can upgrade to now. No further changes required.\n\n### Workarounds\n\nAlternatively,
  a maximum length can be enforced on input strings to shescape to reduce the impact
  of the vulnerability. It is not recommended to try and detect vulnerable input strings,
  as the logic for this may end up being vulnerable to ReDoS itself.\n\n### References\n\n-
  Shescape commit [552e8ea](https://github.com/ericcornelissen/shescape/commit/552e8eab56861720b1d4e5474fb65741643358f9)\n-
  Shescape Release [v1.6.1](https://github.com/ericcornelissen/shescape/releases/tag/v1.6.1)\n\n###
  For more information\n\n- Comment on commit 
  [552e8ea](https://github.com/ericcornelissen/shescape/commit/552e8eab56861720b1d4e5474fb65741643358f9)\n-
  Open an issue at [https://github.com/ericcornelissen/shescape/issues](https://github.com/ericcornelissen/shescape/issues?q=is%3Aissue+is%3Aopen)
  (New issue > Question > Get started)\n"
date: "2022-10-26"
pubdate: "2022-10-25"
affected_range: ">=1.5.10 <1.6.1"
fixed_versions:
- "1.6.1"
affected_versions: "All versions starting from 1.5.10 before 1.6.1"
not_impacted: "All versions before 1.5.10, all versions starting from 1.6.1"
solution: "Upgrade to version 1.6.1 or above."
urls:
- "https://github.com/ericcornelissen/shescape/security/advisories/GHSA-cr84-xvw4-qx3c"
- "https://github.com/advisories/GHSA-cr84-xvw4-qx3c"
uuid: "ec68d830-8a69-4419-a89a-4c9384cbed20"
cwe_ids:
- "CWE-1035"
- "CWE-937"
